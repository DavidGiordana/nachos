// synch.cc
//	Routines for synchronizing threads.  Three kinds of
//	synchronization routines are defined here: semaphores, locks
//   	and condition variables (the implementation of the last two
//	are left to the reader).
//
// Any implementation of a synchronization routine needs some
// primitive atomic operation.  We assume Nachos is running on
// a uniprocessor, and thus atomicity can be provided by
// turning off interrupts.  While interrupts are disabled, no
// context switch can occur, and thus the current thread is guaranteed
// to hold the CPU throughout, until interrupts are reenabled.
//
// Because some of these routines might be called with interrupts
// already disabled (Semaphore::V for one), instead of turning
// on interrupts at the end of the atomic operation, we always simply
// re-set the interrupt state back to its original value (whether
// that be disabled or enabled).
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "synch.h"
#include "system.h"

//----------------------------------------------------------------------
// Semaphore::Semaphore
// 	Initialize a semaphore, so that it can be used for synchronization.
//
//	"debugName" is an arbitrary name, useful for debugging.
//	"initialValue" is the initial value of the semaphore.
//----------------------------------------------------------------------

Semaphore::Semaphore(const char* debugName, int initialValue) {
    name = debugName;
    value = initialValue;
    queue = new List<Thread*>;
}

//----------------------------------------------------------------------
// Semaphore::Semaphore
// 	De-allocate semaphore, when no longer needed.  Assume no one
//	is still waiting on the semaphore!
//----------------------------------------------------------------------

Semaphore::~Semaphore() {
    delete queue;
}

//----------------------------------------------------------------------
// Semaphore::P
// 	Wait until semaphore value > 0, then decrement.  Checking the
//	value and decrementing must be done atomically, so we
//	need to disable interrupts before checking the value.
//
//	Note that Thread::Sleep assumes that interrupts are disabled
//	when it is called.
//----------------------------------------------------------------------

void Semaphore::P() {
    IntStatus oldLevel = interrupt->SetLevel(IntOff);	// disable interrupts

    while (value == 0) { 			        // semaphore not available
        queue->Append(currentThread);		// so go to sleep
        currentThread->Sleep();
    }
    value--; 					            // semaphore available,
                                            // consume its value

    interrupt->SetLevel(oldLevel);		// re-enable interrupts
}

//----------------------------------------------------------------------
// Semaphore::V
// 	Increment semaphore value, waking up a waiter if necessary.
//	As with P(), this operation must be atomic, so we need to disable
//	interrupts.  Scheduler::ReadyToRun() assumes that threads
//	are disabled when it is called.
//----------------------------------------------------------------------

void Semaphore::V() { 
    Thread *thread;
    IntStatus oldLevel = interrupt->SetLevel(IntOff);

    thread = queue->Remove();
    if (thread != NULL) {	   // make thread ready, consuming the V immediately
        scheduler->ReadyToRun(thread);
    }
    value++;
    interrupt->SetLevel(oldLevel);
}

//----------------------------------------------------------------------
// Lock::Lock(const char* debugName)
// Constructor del lock
//
// debugName -> Nombre del lock
//----------------------------------------------------------------------

Lock::Lock(const char* debugName) {
	name = debugName;
	semaph = new Semaphore(debugName, 1);
}

//----------------------------------------------------------------------
// Lock::~Lock()
// Destructor del lock
//----------------------------------------------------------------------
Lock::~Lock() {
	delete semaph;
}


//----------------------------------------------------------------------
// Lock::Acquire()
// Activa el lock
//----------------------------------------------------------------------
void Lock::Acquire() {
	semaph->P();
	hilo = currentThread;
}

//----------------------------------------------------------------------
// Lock::Release()
// Libera el lock
//----------------------------------------------------------------------
void Lock::Release() {
	if ((isHeldByCurrentThread()) && (hilo != NULL)){
		semaph->V();
		hilo = NULL;
	}
}

//----------------------------------------------------------------------
// Lock::isHeldByCurrentThread()
// Comprueba si el hilo actual es el hilo que tiene el lock
// en caso de serlo retorna true
//----------------------------------------------------------------------
bool Lock::isHeldByCurrentThread(){
	return (currentThread == hilo);
}

//----------------------------------------------------------------------
// Condition(const char* debugName, Lock* conditionLock)
// Constructor de las variables de condicion
//
// debugName -> Nombre del lock
// conditionLock -> Lock utilizado por la variable de condicion
//----------------------------------------------------------------------
Condition::Condition(const char* debugName, Lock* conditionLock) { 
	name = debugName;
	queue = new List<Semaphore*>();
	lock = conditionLock;
}

//----------------------------------------------------------------------
// Condition::~Condition()
// Destructor de la variable de condicion
//----------------------------------------------------------------------
Condition::~Condition() {
	delete queue;
	delete lock;
}

//----------------------------------------------------------------------
// Condition::Wait()
// Pausa la ejecucion del hilo hasta recibir una señal
//----------------------------------------------------------------------
void Condition::Wait() { 
	Semaphore *s = new Semaphore("test", 0);
	queue -> Append(s);
	lock -> Release();
	s -> P();
	lock -> Acquire();

	//ASSERT(false); 
}

//----------------------------------------------------------------------
// Condition::Signal()
// Envia una señal para desbloquear un hilo en espera
//----------------------------------------------------------------------
void Condition::Signal() { 
	if(!queue -> IsEmpty()){
		Semaphore *s = queue-> Remove();
		s-> V();
	}
}

//----------------------------------------------------------------------
// Condition::Broadcast()
// Envia una señal para desbloquear todos los hilos en espera
//----------------------------------------------------------------------
void Condition::Broadcast() {
	while(!queue ->IsEmpty()){
		Semaphore *s = queue-> Remove();
		s-> V();
	}
}

//----------------------------------------------------------------------
// Port(const char* name)
// Constructor de un puerto
//
// debugName -> Nombre del lock
//----------------------------------------------------------------------
Port::Port(const char* debugName){
	lock = new Lock("lock mensajes");
	BufferFull = new Condition("BufferFull", lock);
	BufferEmpty = new Condition("BufferEmpty", lock);
	buffer = -1;
	name = debugName;
	isUtil = false;
}

//----------------------------------------------------------------------
// Port::~Port()
// Destructor de puerto
//----------------------------------------------------------------------
Port::~Port(){
	delete lock;
	delete BufferFull;
	delete BufferEmpty;
}

//----------------------------------------------------------------------
// Port::Send(int msj)
// Envia un numero entero como mensaje, el llamado es bloqueante
//
// msj -> Entero enviado
//----------------------------------------------------------------------
void Port::Send(int msj){
	lock->Acquire();
	//Si buffer tiene un valor
	if(isUtil){
		BufferFull->Wait();
	}
	buffer=msj;
	isUtil = true;
	lock->Release();
	BufferEmpty->Signal();     
}

//----------------------------------------------------------------------
// Port::Receive(int *msj)
// Recibe un mensaje, el llamado es bloqueante
//
// msj-> puntero a entero para almacenar el numero recibido
//----------------------------------------------------------------------
void Port::Receive(int *msj){
	lock->Acquire();
	//Si buffer está vacio
	if(!isUtil){
		BufferEmpty->Wait();
	}
	*msj = buffer;
	isUtil = false;
	lock->Release();
	BufferFull->Signal();
}
