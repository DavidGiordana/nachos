// threadtest.cc
//	Simple test case for the threads assignment.
//
//	Create several threads, and have them context switch
//	back and forth between themselves by calling Thread::Yield,
//	to illustrate the inner workings of the thread system.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.
//
// Parts from Copyright (c) 2007-2009 Universidad de Las Palmas de Gran Canaria
//

#include "copyright.h"
#include "system.h"
#include "synch.h"

//Puerto para el envio de mensajes
Port *p = NULL;

//Simula a un mensajero que envia mensajes
void prod (void*z){
	for(int i = 1; i < 10; i++){
		p-> Send(i);
		DEBUG('p', "hilo %s manda %d\n", currentThread->getName(), i);
	}
}

//Simula a un receptor de mensajes
void cons(void*z){
	int m;
	while(1){
		p->Receive(&m);
		DEBUG('p', "Hilo %s recibe %d \n", currentThread->getName(), m);
	}
}

//Metodo llamado en la prueba del ejercicio 3
void pruebaJoin(void*z){
	for(int i=0;i<10;i++)
		DEBUG('p', "Hilo %s iteracion %d \n", currentThread->getName(), i+1);
}

//Prueba para ejercicio 4
void pruebaEj4(void*z){
	for(int i=0;i<10;i++){
		DEBUG('p', "Hilo %s iteracion %d \n", currentThread->getName(), i+1);
		currentThread->Yield();
	}
}
// --------------------------------------------------------

// Ejercicio 2 prueba 
void ejercicio2() {
	p = new Port("test");
	Thread *t= new Thread ("prod");
	t->Fork(prod, NULL);
	cons(NULL);
}

//Ejercicio 3 prueba
void ejercicio3() {
	Thread *t3= new Thread ("Ej3", true);
	t3->Fork(pruebaJoin, NULL);
	t3->Join();
	pruebaJoin(NULL);
}

//Ejercicio 4 prueba
void ejercicio4() {
	Thread *p0= new Thread ("Prioridad 0", 0);
	Thread *p3= new Thread ("Prioridad 3", 3);
	p3->Fork(pruebaJoin, NULL);
	p0->Fork(pruebaJoin, NULL);
}

// --------------------------------------------------------

//Inicializa la prueba
void ThreadTest(){
	ejercicio2();
	//ejercicio3();
	//ejercicio4();
}