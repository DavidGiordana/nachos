#!/bin/bash

# Descarga el archivo si es necesario
if [[ ! -f "gcc-mips.tar.gz" ]]; then
    wget https://svn.dcc.fceia.unr.edu.ar/svn/lcc/R-412/Public/gcc-mips.tar.gz
fi

# Elimina versiones previas
if [[ -d "lib" ]]; then
    rm -rf "lib"
fi 
if [[ -d "mips-dec-ultrix42" ]]; then
    rm -rf "mips-dec-ultrix42"
fi 

# Instala
tar xvzf gcc-mips.tar.gz
mkdir $PWD/lib
ln -s $PWD/mips-dec-ultrix42 $PWD/lib/gcc-lib
echo -e "\n\nAhora debe reemplazar en test/Makefile la variable GCCDIR para que apunte a $PWD/mips-dec-ultrix42/mips- \n\n"